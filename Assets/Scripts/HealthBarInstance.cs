﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarInstance : MonoBehaviour
{
    public Image healthImage;
    bool dropped = false;
    
    public void Drop()
    {
        if(dropped)
        {
            return;
        }
        dropped = true;
        healthImage.DOFade(0f, 0.25f);
        healthImage.rectTransform.DOAnchorPosY(-50f, 0.5f).SetRelative();
    }
}
