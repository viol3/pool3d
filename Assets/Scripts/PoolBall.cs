﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolBall : MonoBehaviour
{
    public float speed = 1f;
    public float rollSpeed = 100f;
    public Collider reflectCollider;
    public Collider triggerCollider;
    public Transform sphereTransform;
    public TrailRenderer trailRenderer;
    
    MeshRenderer ballRenderer;
    Rigidbody _rigidbody;
    MaterialPropertyBlock mpb;
    Vector3 currentVelocity;

    bool scaling = false;
    bool stopped = false;
    // Start is called before the first frame update
    void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        ballRenderer = GetComponentInChildren<MeshRenderer>();
        _rigidbody.velocity = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f)).normalized * speed;
        currentVelocity = _rigidbody.velocity;
        int randomIndex = ColorManager.Instance.GetRandomIndex();

        ballRenderer.material = ColorManager.Instance.GetRandomBallColorMaterial(randomIndex);
        trailRenderer.colorGradient = ColorManager.Instance.GetRandomBallGradient(randomIndex);
    }

    //private void Update()
    //{
    //    Roll();
    //}

    public void StopPhase()
    {
        if(stopped)
        {
            return;
        }
        stopped = true;
        _rigidbody.isKinematic = true;
        _rigidbody.velocity = Vector3.zero;
        reflectCollider.isTrigger = true;
        gameObject.layer = LayerMask.NameToLayer("Default");
    }

    public void StartScalePhase()
    {
        scaling = true;
        reflectCollider.enabled = false;
        triggerCollider.enabled = true;
        ballRenderer.material = Instantiate(ballRenderer.material);
        ballRenderer.material.DOFade(0f, 2.5f);
        Color emissionColor = ballRenderer.material.GetColor("_EmissionColor");
        emissionColor *= -0.25f;
        ballRenderer.material.DOColor(emissionColor, "_EmissionColor", 2.5f);
        transform.DOScale(5f, 2.5f);
        StartCoroutine(DeactivateTrail(0.5f));
        StartCoroutine(DestroyTask(2.5f));
        PoolGameManager.Instance.ResetWaitingLastBall();
    }

    public bool IsScaling()
    {
        return scaling;
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(scaling)
        {
            return;
        }
        if (collision.gameObject.name.Contains("side"))
        {
            Vector3 reflect = Vector3.Reflect(currentVelocity, collision.contacts[0].normal);
            _rigidbody.velocity = reflect.normalized * speed;
            currentVelocity = _rigidbody.velocity;
            //sphereTransform.LookAt(sphereTransform.position + currentVelocity);
            //sphereTransform.eulerAngles = new Vector3(sphereTransform.eulerAngles.x, sphereTransform.eulerAngles.y, -90f);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if(!scaling)
        {
            return;
        }
        PoolBall otherBall = other.GetComponent<PoolBall>();
        if (otherBall && !otherBall.IsScaling())
        {
            otherBall.StartScalePhase();
        }
    }

    IEnumerator DeactivateTrail(float delay)
    {
        yield return new WaitForSeconds(delay);
        trailRenderer.gameObject.SetActive(false);
    }

    IEnumerator DestroyTask(float waitSeconds)
    {
        yield return new WaitForSeconds(waitSeconds);
        PoolGameManager.Instance.RemoveBall(this);
        Destroy(gameObject);
    }

    //void Roll()
    //{
    //    if(!stopped)
    //    {
    //        sphereTransform.Rotate(Vector3.up * rollSpeed * Time.deltaTime);
    //    }
    //}

}
