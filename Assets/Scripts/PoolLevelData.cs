﻿using UnityEngine;

[CreateAssetMenu(fileName = "PoolLevel", menuName = "Pool3D/PoolLevel", order = 1)]
public class PoolLevelData : ScriptableObject
{
    public int ballCount;
    public float ballScale;
    public float ballSpeed;
    public int maxTryCount;
}