﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelLoader : Singleton<LevelLoader>
{
    public PoolLevelData[] levels;
    public int currentLevelIndex = 0;

    public PoolLevelData GetCurrentLevel()
    {
        return levels[currentLevelIndex];
    }

    public void IncreaseLevelIndex()
    {
        currentLevelIndex = (currentLevelIndex + 1) % levels.Length;
    }

    
}
