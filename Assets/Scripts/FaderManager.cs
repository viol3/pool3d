﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FaderManager : MonoBehaviour
{
    public Image faderImage;

    public static FaderManager Instance;
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }

    }

    public IEnumerator OpenTheatre()
    {
        faderImage.DOKill();
        yield return faderImage.DOFade(0f, 0.25f).WaitForCompletion();
        faderImage.raycastTarget = false;
    }

    public IEnumerator CloseTheatre()
    {
        faderImage.DOKill();
        faderImage.raycastTarget = true;
        yield return faderImage.DOFade(1f, 0.25f).WaitForCompletion();

    }
}
