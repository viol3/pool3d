﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PoolGameManager : MonoBehaviour
{
    public Transform ballParent;
    public Transform tryInstanceParent;
    public GameObject tryInstancePrefab;
    public Text finishText;
    public Text nextLevelText;
    public GameObject finishPanelGO;
    public GameObject starPanelGO;

    List<HealthBarInstance> healthBarInstances;
    List<PoolBall> balls;
    int tryCount = 0;
    Coroutine waitingLastBallCo;
    bool finished = false;
    
    public static PoolGameManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    // Start is called before the first frame update
    IEnumerator Start()
    {
        Application.targetFrameRate = 60;
        balls = new List<PoolBall>();
        PoolLevelData levelData = LevelLoader.Instance.GetCurrentLevel();
        tryCount = levelData.maxTryCount;
        GenerateBalls(levelData);
        healthBarInstances = new List<HealthBarInstance>();
        for (int i = 0; i < tryCount; i++)
        {
            HealthBarInstance hbi = Instantiate(tryInstancePrefab, tryInstanceParent).GetComponent<HealthBarInstance>();
            healthBarInstances.Add(hbi);
        }
        yield return FaderManager.Instance.OpenTheatre();
        
    }

    // Update is called once per frame
    void Update()
    {
        if(finished)
        {
            return;
        }
        if(Input.GetMouseButtonDown(0))
        {
            if(tryCount <= 0)
            {
                return;
            }
            RaycastHit hitInfo;
            if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hitInfo))
            {
                PoolBall ball = GetBallOnMouse();
                if(ball && !ball.IsScaling())
                {
                    StopAllBalls();
                    ball.StartScalePhase();
                    DropHealthBarInstance();
                    tryCount--;
                }
            }
        }
    }

    void GenerateBalls(PoolLevelData levelData)
    {
        for (int i = 0; i < levelData.ballCount; i++)
        {
            PoolBall ball = Instantiate(PrefabReferencer.Instance.ballPrefab, ballParent).GetComponent<PoolBall>();
            ball.transform.position = new Vector3(Random.Range(-2.7f, 2.7f), ball.transform.position.y, Random.Range(-3.7f, 3.7f));
            ball.transform.localScale = Vector3.one * levelData.ballScale;
            ball.speed = levelData.ballSpeed;
            balls.Add(ball);
        }
    }

    PoolBall GetBallOnMouse()
    {
        RaycastHit[] hits = Physics.RaycastAll(Camera.main.ScreenPointToRay(Input.mousePosition));
        for (int i = 0; i < hits.Length; i++)
        {
            if(hits[i].collider.GetComponent<PoolBall>())
            {
                return hits[i].collider.GetComponent<PoolBall>();
            }
        }
        return null;
    }

    void DropHealthBarInstance()
    {
        healthBarInstances[0].Drop();
        healthBarInstances.RemoveAt(0);
    }
    

    void StopAllBalls()
    {
        for (int i = 0; i < balls.Count; i++)
        {
            balls[i].StopPhase();
        }
    }

    public void RemoveBall(PoolBall ball)
    {
        balls.Remove(ball);
    }

    public void OnNextLevelButtonClick()
    {
        StartCoroutine(LoadSceneTask("Play"));
    }

    IEnumerator LoadSceneTask(string sceneName)
    {
        yield return FaderManager.Instance.CloseTheatre();
        SceneManager.LoadScene(sceneName);
    }

    public void ResetWaitingLastBall()
    {
        if(waitingLastBallCo != null)
        {
            StopCoroutine(waitingLastBallCo);
        }
        waitingLastBallCo = StartCoroutine(WaitForLastBall());
    }

    IEnumerator WaitForLastBall()
    {
        yield return new WaitForSeconds(3f);
        if(tryCount == 0)
        {
            if (balls.Count == 0)
            {
                finishText.text = "PERFECT";
                nextLevelText.text = "NEXT LEVEL";
                LevelLoader.Instance.IncreaseLevelIndex();
                starPanelGO.SetActive(true);
            }
            else
            {
                finishText.text = "FAILED";
                nextLevelText.text = "TRY AGAIN";
                starPanelGO.SetActive(false);
            }
            finishPanelGO.SetActive(true);
            finished = true;
        }
        else if (balls.Count == 0)
        {
            finishText.text = "PERFECT";
            nextLevelText.text = "NEXT LEVEL";
            LevelLoader.Instance.IncreaseLevelIndex();
            finishPanelGO.SetActive(true);
            starPanelGO.SetActive(true); 
            finished = true;
        }

    }
}
