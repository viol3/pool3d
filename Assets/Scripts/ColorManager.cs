﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour
{
    public Material[] ballColorMaterials;
    public Gradient[] ballColorGradients;
    public static ColorManager Instance;
    private void Awake()
    {
        Instance = this;
    }
    
    public Material GetRandomBallColorMaterial(int index)
    {
        return ballColorMaterials[index];
    }

    public Gradient GetRandomBallGradient(int index)
    {
        return ballColorGradients[index];
    }

    public int GetRandomIndex()
    {
        return Random.Range(0, ballColorMaterials.Length);
    }
}
