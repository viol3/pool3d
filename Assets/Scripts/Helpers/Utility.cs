﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Utility
{
    public static float GetAngleFromTwoPoints(Vector3 p1, Vector3 p2)
    {
        float result = Mathf.Atan2(p2.z - p1.z, p2.x - p1.x) * 180 / Mathf.PI;
        return -result + 90f;
    }
    public static bool ApproximatelyColor(Color color1, Color color2, int threshold = 1)
    {
        int r1 = (int)(color1.r * 255f);
        int r2 = (int)(color2.r * 255f);
        int g1 = (int)(color1.g * 255f);
        int g2 = (int)(color2.g * 255f);
        int b1 = (int)(color1.b * 255f);
        int b2 = (int)(color2.b * 255f);
        int a1 = (int)(color1.a * 255f);
        int a2 = (int)(color2.a * 255f);
        if (Mathf.Abs(r1 - r2) > threshold)
        {
            return false;
        }
        if (Mathf.Abs(g1 - g2) > threshold)
        {
            return false;
        }
        if (Mathf.Abs(b1 - b2) > threshold)
        {
            return false;
        }
        if (Mathf.Abs(a1 - a2) > threshold)
        {
            return false;
        }
        return true;
    }
    public static void ChangeSpriteAlpha(SpriteRenderer sr, float alpha)
    {
        Color c = sr.color;
        c.a = alpha;
        sr.color = c;
    }

    public static GameObject GetColliderAt(Camera camera, string name)
    {
        RaycastHit[] hits = Physics.RaycastAll(camera.ScreenPointToRay(Input.mousePosition));
        for (int i = 0; i < hits.Length; i++)
        {
            if (hits[i].collider.name.Equals(name))
            {
                return hits[i].collider.gameObject;
            }
        }
        return null;
    }

    public static GameObject GetUIElementByObject(GraphicRaycaster gr, Vector3 pos, GameObject go)
    {
        PointerEventData m_PointerEventData = new PointerEventData(null);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = pos;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        gr.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
            if (result.gameObject == go)
            {
                return result.gameObject;
            }
        }
        return null;
    }

    public static GameObject GetUIElementByName(GraphicRaycaster gr, Vector3 pos, string objectName)
    {
        PointerEventData m_PointerEventData = new PointerEventData(null);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = pos;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        gr.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        foreach (RaycastResult result in results)
        {
            if (result.gameObject.name == objectName)
            {
                return result.gameObject;
            }
        }
        return null;
    }

    public static bool IsThereAnyUIElement(GraphicRaycaster gr, Vector3 pos)
    {
        PointerEventData m_PointerEventData = new PointerEventData(null);
        //Set the Pointer Event Position to that of the mouse position
        m_PointerEventData.position = pos;

        //Create a list of Raycast Results
        List<RaycastResult> results = new List<RaycastResult>();

        //Raycast using the Graphics Raycaster and mouse click position
        gr.Raycast(m_PointerEventData, results);

        //For every result returned, output the name of the GameObject on the Canvas hit by the Ray
        return results.Count > 0;
    }

    public static Vector3 MouseWorldPosition()
    {
        Vector3 result = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        result.z = 0f;
        return result;
    }

    public static void ChangeAlphaImage(Image image, float alpha)
    {
        Color c = image.color;
        c.a = alpha;
        image.color = c;
    }

    public static void ChangeAlphaImage(RawImage image, float alpha)
    {
        Color c = image.color;
        c.a = alpha;
        image.color = c;
    }

    public static void ChangeAlphaText(Text text, float alpha)
    {
        Color c = text.color;
        c.a = alpha;
        text.color = c;
    }

    public static string GetRawNameFromFileName(string fileName)
    {
        for (int i = 0; i < fileName.Length; i++)
        {
            if (fileName[i] == '.')
            {
                return fileName.Substring(0, i);
            }
        }
        return "Image Entity";
    }

    public static void SetNativeSize(RawImage image)
    {
        float ratio = (float)image.texture.width / (float)image.texture.height;
        if (ratio > 1f)
        {
            image.rectTransform.sizeDelta = new Vector2(100, 100 / ratio);
        }
        else
        {
            image.rectTransform.sizeDelta = new Vector2(100 * ratio, 100);
        }
    }



    public static GameObject GetUIElementAt(GraphicRaycaster gr, string tag)
    {
        List<RaycastResult> resultList = new List<RaycastResult>();
        PointerEventData data = new PointerEventData(null);
        data.position = Input.mousePosition;
        gr.Raycast(data, resultList);
        for (int i = 0; i < resultList.Count; i++)
        {
            if (resultList[i].gameObject.tag == tag)
            {
                return resultList[i].gameObject;
            }
        }
        return null;
    }

    public static T GetUIElementAt<T>(GraphicRaycaster gr) where T : Component
    {
        List<RaycastResult> resultList = new List<RaycastResult>();
        PointerEventData data = new PointerEventData(null);
        data.position = Input.mousePosition;
        gr.Raycast(data, resultList);
        for (int i = 0; i < resultList.Count; i++)
        {
            if (resultList[i].gameObject.GetComponent<T>())
            {
                return resultList[i].gameObject.GetComponent<T>();
            }
        }
        return null;
    }



    public static void Shuffle<T>(ref List<T> ts)
    {
        var count = ts.Count;
        var last = count - 1;
        for (var i = 0; i < last; ++i)
        {
            var r = UnityEngine.Random.Range(i, count);
            var tmp = ts[i];
            ts[i] = ts[r];
            ts[r] = tmp;
        }
    }

    public static Vector3 GetWorldPosFromRectPos(Vector3 pos, Transform parent)
    {
        return parent.TransformPoint(pos);
    }

    public static Vector2 GetRectPosFromWorldPos(Vector3 pos, Transform parent)
    {
        return parent.InverseTransformPoint(pos);
    }

}
